type Food = {
    type : string;
    foodDetailList: FoodDetail[];
}

type FoodDetail = {
    name: string;
    prices: FoodPrice[];
}

type FoodPrice = {
    location: string;
    priceHkd: number;
}

export const foodLocationList: String[] = [
    "Wellcome",
    "ParknShop",
]

export const foods: Food[] = [
    {
        type: "Snacks",
        foodDetailList: [
            {
                name: "Mint Chocolate",
                prices: [
                    {
                        location: "Wellcome",
                        priceHkd: 13
                    },
                    {
                        location: "ParknShop",
                        priceHkd: 15
                    }
                ]
            }
        ]
    },
    {
        type: "Meat",
        foodDetailList: [
            {
                name: "Pork",
                prices: [
                    {
                        location: "Wellcome",
                        priceHkd: 13
                    },
                    {
                        location: "ParknShop",
                        priceHkd: 15
                    }
                ]
            }
        ]
    }
];