import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import PriceTable from './scenes/PriceTable';
import { store } from './app/store';
import { Provider } from 'react-redux';


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PriceTable category="Food" type=""/>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
