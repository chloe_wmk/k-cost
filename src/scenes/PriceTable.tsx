import React from 'react';
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Paper } from '@mui/material';
import { foodLocationList, foods } from '../data/foods';

// useEffect(() => {
//     dataList = createDataList()
//   }, []);

// const [dataList, setDataList] = useState([]);

export interface Props {
  category: string;
  type: string;
}

export default class PriceTable extends React.Component<Props, object> {

  tName:string = "Name"

  render() {
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell key="name">{this.tName}</TableCell>
                {
                    foodLocationList.map((foodLoc, i) => {
                        return <TableCell key={i}>{foodLoc}</TableCell>
                    })
                }
            </TableRow>
          </TableHead>
          <TableBody>
            {this.contentRow()}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  contentRow = () => {
    return (
      foods.map((food) => {
        return ( 
          food.foodDetailList.map((foodDetail) => {
            let locationPriceMap = new Map();
    
            foodDetail.prices.map((locPrice) => {
              locationPriceMap.set(locPrice.location, locPrice.priceHkd);
            })
            return ( 
              <TableRow key={foodDetail.name}>
                <TableCell component="th" scope="row">{foodDetail.name}</TableCell>
                {
                  foodLocationList.map((foodLocation) => {
                    let price = locationPriceMap.get(foodLocation);
                    return <TableCell key={price}>{price}</TableCell>
                  })
                }
              </TableRow> 
            )
          })
        )
      })
    )
  }
}